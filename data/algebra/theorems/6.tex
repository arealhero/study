<h1>6. Закон внешней композиции для векторных пространств. Теоремы о нулях и обратных элементах</h1>

<h2>Закон внешней композиции для векторных пространств</h2>

<p>
	<b>Определение.</b>
	<i>
		Отображение \(K\) называется законом внешней композиции, если

		\begin{equation*}
		    K : A \subseteq P \times G \to G.
		\end{equation*}

		\(P\) именуется множеством (или областью) операторов закона,
		элементы \(P\) &mdash; операторы закона, значение \(K(p, g)\)
		&mdash; композицией \(p\) и \(g\) относительно этого закона.
	</i>
</p>

<p>
	<b>Определение.</b>
	<i>
		Векторным или линейным пространством (ВП или ЛП, соответственно)
		называется абелева группа, связанная с некоторым полем,
		именуемым полем операторов, законом внешней композиции,
		удовлетворяющим следующим аксиомам.
	</i>
</p>

<h3>Аксиомы закона внешней композиции для ВП</h3>

<p>
	<ol type="I">
		<li>
			Дистрибутивность для сложения в поле:

			\begin{equation*}
				(\alpha + \beta) x = \alpha x + \beta x \quad
				\forall \alpha, \beta \in P, \; \forall x \in G.
			\end{equation*}

			(Здесь первый "\(+\)" &mdash; сложение в поле,
			второй &mdash; сложение в группе)
		</li>

		<li>
			Дистрибутивность для сложения в группе:

			\begin{equation*}
				\alpha(x + y) = \alpha x + \alpha y \quad
				\forall \alpha \in P, \; \forall x, y \in G.
			\end{equation*}

			(Здесь оба "\(+\)" &mdash; сложение в группе)
		</li>

		<li>
			\((\alpha \beta) x = \alpha(\beta x) \quad
			\forall \alpha, \beta \in P, \; \forall x \in G\).

			(В левом выражении первое "пусто" &mdash; умножение в поле,
			второе &mdash; закон внешней композиции. В правом выражении
			обе "пустоты" &mdash; закон внешней композиции)
		</li>

		<li>
			\(1 \cdot x = x \quad \forall x \in G, \; 1 \in P\)
			&mdash; нейтральный элемент.
		</li>
	</ol>
</p>

<p>
	Если \(P\) &mdash; не поле, а <i>тело</i>, то появляются понятия
	<i>левое векторное пространство</i> и <i>правое векторное пространство</i>.
	Аксиома III &mdash; левое ВП. Для правого принимается аксиома:
</p>

<p style="text-indent: 20px">
	III'. \((\alpha \beta) x = \beta (\alpha x) \quad
	\forall \alpha, \beta \in , \; \forall x \in G\).
</p>

<p>
	Элементы ВП называются векторами (т.е. элементы \(G\)).
</p>

<p>
	<b>Определение.</b>
	<i>
		Если \(P\) &mdash; кольцо, то аксиомы I + II + III и I + II + III'
		дают левый и правый модуль соответственно. Названия:
		\(P\)-модуль \(G\), кольцо операторов \(P\).

		<br>
		<br>

		Если \(P\) &mdash; кольцо с единицей, то левый и правый модули
		будут унитарными при выполнении аксиомы IV.
	</i>
</p>

<p>
	<b>Определение.</b>
	<i>
		Линейное подпространство &mdash; это подмножество, содержащееся
		в векторном пространстве над полем \(P\), для которого операции
		сложения элементов и умножения на любой оператор из области
		операторов \(P\) не выводят из этого подпространства.
		Иными словами: пусть \(V\) &mdash; векторное пространство.
		Множество \(L\) называется его подпространством, когда

		\begin{equation}
		    V \supset L \land
		    \begin{cases}
			(\forall x, y \in L) \quad x + y \in L \\
			(\forall x \in L) \; (\forall t \in P) \quad tx \in L
		    \end{cases}
		\end{equation}

		Аналогично. Структура \(S\) называется подмодулем
		\(K\)-модуля \(G\), если

		\begin{equation}
		    G \supset S \land
		    \begin{cases}
			(\forall x, y \in S) \quad x + y \in S \\
			(\forall x \in S) \; (\forall t \in K) \quad tx \in S
		    \end{cases}
		\end{equation}
	</i>
</p>

<h2>Теоремы о нулях</h2>

<p>
	Будем далее обозначать через \(\overline{0}\) нейтральный элемент ВП
	или модуля, а через \(0\) &mdash; нейтральный элемент аддитивной группы
	поля или кольца.
</p>

<p id="theorem-1">
	<b>Теорема 1.</b>
	<i>
		Пусть есть \(R\)-модуль \(G\): \(G\) &mdash;
		абелева группа, \(R\) &mdash; кольцо. Также пусть
		\(0 \in R, \overline{0} \in G\). Тогда
		\(0a = \overline{0} \quad \forall a \in G\).
	</i>
</p>

<p>
	<i>Доказательство.</i> \(0a + 0a = (0 + 0) a = 0a\) влечёт \(0a\)
	&mdash; нейтральный элемент по отношению к сложению для элемента \(0a\).
	Но было доказано, что в группе нейтральный элемент единственный и один и
	тот же для всех элементов группы. Т.е. \(0a = \overline{0}\).
	\(\blacksquare\)
</p>

<p id="theorem-2">
	<b>Теорема 2.</b>
	<i>
		Пусть \(G\) &mdash; абелева группа, \(R\) &mdash; кольцо.
		Также пусть \(\overline{0} \in G\) (ноль группы) и \(0 \in R\). Тогда \(\alpha\overline{0} = \overline{0} \; \forall \alpha \in R\).
	</i>
</p>

<p>
	<i>Доказательство.</i> \(\alpha \overline{0} + \alpha \overline{0} =
	\alpha(\overline{0} + \overline{0}) = \alpha \overline{0} \implies
	\alpha \overline{0}\) &mdash; нулевой элемент для элемента \(\alpha
	\overline{0}\) (и правый, и левый, так как \(G\) &mdash; абелева группа)
	\(\implies \alpha \overline{0} = \overline{0}\). \(\blacksquare\)
</p>

<p id="theorem-3">
	<b>Теорема 3.</b>
	<i>
		Пусть \(a \in G\) (\(G\) &mdash; абелева группа) и
		\(\alpha \in R\) (\(R\) &mdash; кольцо). Тогда

		\begin{equation*}
		    (-\alpha)a = -(\alpha a).
		\end{equation*}
	</i>
</p>

<p>
	<i>Доказательство.</i> \((-\alpha) a + \alpha a = (-\alpha + \alpha) a
	= 0a = \overline{0}\). То есть \((-\alpha) a\) является обратным к
	\(\alpha a\) элементом. А обозначение обратного элемента такое:
	\(-(\alpha a)\). \(\blacksquare\)
</p>

<p id="theorem-4">
	<b>Теорема 4.</b>
	<i>
		Пусть \(a \in G, \; \alpha \in R\).
		Тогда \(\alpha (-a) = -(\alpha a)\).
	</i>
</p>

<p>
	<i>Доказательство.</i> \(\alpha(-a) + \alpha a = \alpha(-a + a)
	= \alpha \overline{0} = \overline{0}\). \(\blacksquare\)
</p>

<p id="theorem-5">
	<b>Теорема 5.</b>
	<i>
		В векторном пространстве \(V\) над полем \(P\) нет ''делителей нуля''
		для закона внешней композиции. Т.е.

		\begin{equation*}
		    \alpha a = \overline{0} \implies
			(\alpha = 0 \lor a = \overline{0}),
		\end{equation*}

		где \(\alpha \in P, \; a \in V\).
	</i>
</p>

<p>
	<i>Доказательство.</i> Пусть \(\alpha a = \overline{0}\) и \(\alpha \neq 0\).
	Тогда, поскольку все элементы поля кроме нуля имеют обратные, существует
	обратный к \(\alpha\), обозначаемый через \(\alpha^{-1}\). Умножим на него
	слева последнее равенство (более строго: произведём внешнюю композицию
	оператора \(\alpha^{-1}\) с обеими частями равенства). В силу аксиомы III
	закона внешней композиции и <a href="#theorem-2">теоремы 2</a>:

	\begin{equation*}
	    \overline{0} = \alpha^{-1} \overline{0} = \alpha^{-1} (\alpha a)
		= (\alpha^{-1} \alpha) a = a. \qquad \blacksquare
	\end{equation*}
</p>

<p id="theorem-6">
	<b>Теорема 6.</b>

	<ol>
		<li>
			При умножении любого элемента поля \(P\) на 0 получится 0;
		</li>

		<li>
			\((\forall a, b \in P) \; (-a) b = -ab\);
		</li>

		<li>
			В поле нет делителей нуля.
		</li>
	</ol>
</p>

<p>
	<b>Замечание.</b>
	<i>
		В кольце могут быть делители нуля.
		<span class="todo">Привести пример.</span>
	</i>
</p>

<p id="theorem-7">
	<b>Теорема 7.</b>
	<i>
		Если в кольце \(K\) есть делители нуля, то они есть и
		в модуле \(M\) над кольцом \(K\), когда в этом модуле
		больше одного элемента.
	</i>
</p>

<p>
	<i>Доказательство.</i> Пусть \(\alpha, \beta \in K \land \alpha,
	\beta \neq 0 \land \alpha \beta = 0\). Пусть \(a \in M\) и \(a \neq 0\).
	Согласно теоремам о нулях, \(\overline{0} = 0 a = (\alpha \beta) a
	= \alpha (\beta a)\). Если \(\beta \alpha = \overline{0}\),
	то \(\beta\) и \(a\) &mdash; делители нуля для закона внешней композиции.
	Иначе делителями нуля для ЗВК являются \(\alpha\) и \(\beta a\).
	\(\blacksquare\)
</p>

<hr>

<h3>Источники</h3>

<ul>
	<li>
		<a href="http://www.apmath.spbu.ru/ru/staff/mikheev/files/algebra2__Copy___35_.pdf">Конспект Михеева (2 часть), страница 4</a>
	</li>
</ul>
