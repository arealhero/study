# Study

### Requirements

* [NodeJS & NPM](https://nodejs.org/en/download/)
* [git](https://git-scm.com/downloads)

### Setup

Clone the repository

```sh
$ git clone https://gitlab.com/arealhero/study
```

Install NodeJS dependencies

```sh
$ cd study/backend
$ npm install .
```

Launch the server

```sh
$ cd ..
$ node backend/src/Server.js
```

Open your browser and go to `http://localhost`

### How to contribute

Create a new branch

```sh
$ git checkout -b %YOUR_BRANCH_NAME%
```

Commit your changes

```sh
$ git commit
```

Push your changes to the repository

```sh
$ git push
```

Create a merge request (via the left-side menu on GitLab)

### Как писать билеты

Структура довольно простая:

* вся разметка происходит посредством HTML.
* формулы в текущую линию добавляются через `\(...\)`
* формулы по центру добавляются через `\[...\]`

### Полезные ссылки

* [List of LaTeX symbols](https://oeis.org/wiki/List_of_LaTeX_mathematical_symbols)
* [Getting started with GitLab](https://towardsdatascience.com/getting-started-with-gitlab-the-absolute-beginners-guide-ea9e5cadac8b)

